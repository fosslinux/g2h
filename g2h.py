#!/usr/bin/env python3
# convert gopher links to html links

import argparse
import os
import sys

def parser():
    parser = argparse.ArgumentParser(description="convert gopher links to html links")
    parser.add_argument("title", required=True,
                        help="title for your site")
    parser.add_argument("gophermap", required=False, default="gophermap",
                        help="path to the gophermap")
    parser.add_argument("html_file", required=False, default="index.html",
                        help="path to the resulting html file")
    return parser.parse_args()

def validate_input(args):
    if not os.path.isfile(args.gophermap):
        print("the gophermap dosen't exist!")
        sys.exit(1)

def linkconv(string):
    string = string.strip('\n')
    string = string.strip(string[0])
    strsplit = string.split('\t')
    if len(strsplit) > 2:
        if strsplit[2] != 'localhost':
            link = '<a href="http://' + strsplit[2] + strsplit[1] + '">' + strsplit[0] + '</a>\n'
        else:
            link = '<a href="' + strsplit[1] + '">' + strsplit[0] + '</a>\n'
    else:
        link = '<a href="' + strsplit[1] + '">' + strsplit[0] + '</a>\n'
    return link


def write_head(entry):
    entry.write('<!DOCTYPE html>\n')
    entry.write('<html>\n')
    entry.write('<head>\n')
    entry.write('<title>\n')
    entry.write(title + '\n')
    entry.write('</title>\n')
    entry.write('<body style="white-space: pre; font-family: \'Courier New\', monospace;">')

def write_body(args, entry):
    with open(args.gophermap) as f:
        for line in f:
            if '\t' in line:
                if line[0] == 'i':
                    line = line.strip(line[0])
                    line = line.split('\t')
                    entry.write(line[0] + '\n')
                else:
                    entry.write(linkconv(line))
            else:
                entry.write(line)

def write_tail(entry):
    entry.write('</body>\n')
    entry.write('</html>\n')

def write_html(args):
    entry = open(args.html_file, 'w')
    write_head(entry)
    write_body(args, entry)
    write_tail(entry)
    entry.close()

def main():
    args = parser()
    validate_input(args)